package com.softserveinc.ita.manygames.model.engine.Reversi;

import com.softserveinc.ita.manygames.model.engine.GameState;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;

/**
 * @author Igor Khlaponin
 */
public class TestReversiTurnLogicValidationPrinciples {

    private Reversi game;
    private Board boardMock;

    @Before
    public void setUpGame(){
        game = new Reversi();
        game.setFirstPlayer("Frank");
    }


    @Test
    public void checkIfWeTurnIntoBusyCell(){
        boardMock = mock(Board.class, Mockito.CALLS_REAL_METHODS);
        String turn = "e4";
        doCallRealMethod().when(boardMock).putPawn(turn, GameState.WAIT_FOR_FIRST_PLAYER_TURN);

        game.makeTurn(game.getFirstPlayer(), turn);

        assertFalse(game.validateTurnLogic(turn));
    }

    @Test
    public void checkIfWeTurnIntoEmptyCellAccordingToTheRulesOfGame(){
        boardMock = mock(Board.class, Mockito.CALLS_REAL_METHODS);
        String turn = "f4";
        doCallRealMethod().when(boardMock).putPawn(turn, GameState.WAIT_FOR_FIRST_PLAYER_TURN);

        game.makeTurn(game.getFirstPlayer(), turn);

        assertTrue(game.validateTurnLogic(turn));
    }

    @Test
    public void checkIfWeTurnIntoEmptyCellRoundedWithEmptyCells() {
        boardMock = mock(Board.class, Mockito.CALLS_REAL_METHODS);
        String turn = "b7";
        doCallRealMethod().when(boardMock).putPawn(turn, GameState.WAIT_FOR_FIRST_PLAYER_TURN);

        game.makeTurn(game.getFirstPlayer(), turn);

        assertFalse(game.validateTurnLogic(turn));

    }

    //TODO wright the test that check if the game has a winner

}
