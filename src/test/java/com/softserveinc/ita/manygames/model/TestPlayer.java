package com.softserveinc.ita.manygames.model;

import com.softserveinc.ita.manygames.utils.PasswordEncryptor;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @author Igor Khlaponin
 */
public class TestPlayer {

    private Player player = null;

    @Before
    public void setUp() {
        player = new Player();
    }

    @Test
    public void checkIfPasswordIsAddingEncrypted() {
        String originalPassword = "123abc";
        player.addPassword(originalPassword);

        assertThat(originalPassword, not(player.getPassword()));
    }

    @Test
    public void checkCorrectPasswordEncryptionDuringCreation() {
        String originalPassword = "123abc";
        player.addPassword(originalPassword);

        assertTrue(PasswordEncryptor.checkPassword(originalPassword, player));
    }

    @Test
    public void checkEncryptedPasswordCreationViaConstructor() {
        String originalPassword = "123321";
        Player player1 = new Player("Gimly", originalPassword);

        assertThat(originalPassword, not(player1.getPassword()));
        assertTrue(PasswordEncryptor.checkPassword(originalPassword, player1));
    }


}
