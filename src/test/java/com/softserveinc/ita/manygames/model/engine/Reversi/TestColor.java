package com.softserveinc.ita.manygames.model.engine.Reversi;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Igor Khlaponin
 */
public class TestColor {

    @Test
    public void checkGetWhiteColor(){
        int expectedColor = Color.WHITE;
        int acturalColor = Color.getColor(1);
        assertEquals(expectedColor, acturalColor);
    }

    @Test
    public void checkGetBlackColor(){
        int expectedColor = Color.BLACK;
        int actualColor = Color.getColor(2);
        assertEquals(expectedColor, actualColor);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void checkGetColorByWrongNumber(){
        Color.getColor(3);
    }

    @Test
    public void checkGetOpponentBlackColorIfCurrentColorIsWhite(){
        int expectedColor = Color.BLACK;
        int actualColor = Color.getOpponentColor(Color.WHITE);
        assertEquals(expectedColor, actualColor);
    }

    @Test
    public void checkGetOpponentWhiteColorIfCurrentColorIsBlack(){
        int expectedColor = Color.WHITE;
        int actualColor = Color.getOpponentColor(Color.BLACK);
        assertEquals(expectedColor, actualColor);
    }

}
