package com.softserveinc.ita.manygames.model.engine.Reversi;

import com.softserveinc.ita.manygames.model.Player;
import com.softserveinc.ita.manygames.model.engine.GameState;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Igor Khlaponin
 */
public class TestReversi {

    private Reversi reversi;
    private Player player1;
    private Player player2;

    @Before
    public void setUp() {
        player1 = mock(Player.class);
        player2 = mock(Player.class);
        when(player1.getName()).thenReturn("Sam");
        when(player2.getName()).thenReturn("Frodo");
        reversi = new Reversi();
    }

    @Test
    public void checkGetGameBoard() {
        assertNotNull(reversi.getGameBoard());
    }

    @Test
    public void checkGetBoardAsJson() {
        String expected = "[{\"x\":3,\"y\":3,\"value\":2}," +
                "{\"x\":4,\"y\":3,\"value\":1}," +
                "{\"x\":3,\"y\":4,\"value\":1}," +
                "{\"x\":4,\"y\":4,\"value\":2}]";
        String actual = reversi.getBoard();
        assertEquals(expected, actual);
    }

    @Test
    public void checkGetCurrentPlayer() {
        reversi.setFirstPlayer("Yoda");
        reversi.setSecondPlayer("Dart Wayder");

        String expected = reversi.getFirstPlayer();
        String actual = reversi.getCurrentPlayer();

        assertEquals(expected, actual);
    }

    @Test
    public void checkIfWeCouldMakeTurn() {
        reversi.setFirstPlayer(player1.getName());
        reversi.setSecondPlayer(player2.getName());

        assertTrue(reversi.makeTurn(player1.getName(), "f4"));
    }

    @Test
    public void checkIfGameIsFinishedAfterLastTurn() {
        int[][] field = new int[8][8];
        for (int i = 0; i< field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                field[i][j] = 1 + (int) (Math.random()*2);
            }
        }
        field[0][0] = 0;

        reversi.setFirstPlayer(player1.getName());
        reversi.setSecondPlayer(player2.getName());
        assertFalse(reversi.isFinished());

        reversi.getGameBoard().setField(field);
        reversi.makeTurn(player1.getName(), "a8");

        assertTrue(reversi.isFinished());
    }

    @Test
    public void checkIfGameIsFinishedAfterWithFirstPlayerAsAWinner() {
        int[][] field = new int[8][8];
        for (int i = 0; i< field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                field[i][j] = 1;
            }
        }
        field[0][0] = 0;

        reversi.setFirstPlayer(player1.getName());
        reversi.setSecondPlayer(player2.getName());
        assertFalse(reversi.isFinished());

        reversi.getGameBoard().setField(field);
        reversi.makeTurn(player1.getName(), "a8");
        int gameState = GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
        assertEquals(gameState, reversi.getCurrentGameState());
    }

    @Test
    public void checkIfGameIsFinishedAfterWithSecondPlayerAsAWinner() {
        int[][] field = new int[8][8];
        for (int i = 0; i< field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                field[i][j] = 2;
            }
        }
        field[0][0] = 0;

        reversi.setFirstPlayer(player1.getName());
        reversi.setSecondPlayer(player2.getName());
        assertFalse(reversi.isFinished());

        reversi.getGameBoard().setField(field);
        reversi.makeTurn(player1.getName(), "a8");
        int gameState = GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
        assertEquals(gameState, reversi.getCurrentGameState());
    }

    @Test
    public void checkTheFirstPlayerAsAWinner() {
        int[][] field = new int[8][8];
        for (int i = 0; i< field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                field[i][j] = 1;
            }
        }
        field[0][0] = 0;

        reversi.setFirstPlayer(player1.getName());
        reversi.setSecondPlayer(player2.getName());
        assertFalse(reversi.isFinished());

        reversi.getGameBoard().setField(field);
        reversi.makeTurn(player1.getName(), "a8");
        assertEquals(player1.getName(), reversi.getTheWinner());
    }

    @Test
    public void checkTheSecondPlayerAsAWinner() {
        int[][] field = new int[8][8];
        for (int i = 0; i< field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                field[i][j] = 2;
            }
        }
        field[0][0] = 0;

        reversi.setFirstPlayer(player1.getName());
        reversi.setSecondPlayer(player2.getName());
        reversi.getGameBoard().setField(field);

        reversi.makeTurn(player1.getName(), "a8");
        assertEquals(player2.getName(), reversi.getTheWinner());
    }

}
