package com.softserveinc.ita.manygames.utils;

import com.softserveinc.ita.manygames.model.Player;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Igor Khlaponin
 */
public class TestPasswordEncryptor {

    private Player player = null;

    @Before
    public void setUp() {
        player = mock(Player.class);
        when(player.getName()).thenReturn("Bilbo");

    }

    @Test
    public void checkPasswordEncryption() {
        String originalPassword = "abc123";
        String encryptedPassword = PasswordEncryptor.getEncryptedPassword(originalPassword);
        when(player.getPassword()).thenReturn(encryptedPassword);

        assertTrue(PasswordEncryptor.checkPassword(originalPassword, player));
    }

    @Test
    public void checkWrongPassword() {
        String originalPassword = "abc123";
        String wrongPassword = "123abc";
        String encryptedPassword = PasswordEncryptor.getEncryptedPassword(originalPassword);
        when(player.getPassword()).thenReturn(encryptedPassword);

        assertFalse(PasswordEncryptor.checkPassword(wrongPassword, player));
    }
}
