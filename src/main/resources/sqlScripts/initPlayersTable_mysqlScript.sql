DROP TABLE IF EXISTS players;

CREATE TABLE players
(
	id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	login VARCHAR(20) NOT NULL,
	password VARCHAR(64),
	email VARCHAR(25),
	fullname VARCHAR(30),
	gender VARCHAR(6),
	birthday_date DATE,
	registration_time TIMESTAMP,
	about VARCHAR(100),
	avatar VARCHAR(100)
);


INSERT INTO players (login, password, email, gender, fullname) /*password = 1111*/
VALUES ('Sam', 'I4tRkyLjcOTNRwUAVfnIz+017/kKu+yLSEFGyo8nXstaQ0xLF99cXMkH0x0DWvhc',
'gamgee@gmail.com', 'MALE', 'Gamgee');

INSERT INTO players (login, password, email, gender, fullname) /*password = 2222*/
VALUES ('Frodo', 'Oy+x64iYNRg865WOMh6RLeO56JKWmuYsHzwe2FfOkX/hoiTp3rAkWum/EtF2ntQ5',
'shire.hole@gmail.com', 'MALE', 'Beggins');

INSERT INTO players (login, password, email, gender, fullname) /*password = 3333*/
VALUES ('Gandalf', 'gwXmufeTM7RVswMz0kZn649J1XjOcHsUDYWr+XFfSfGCX5+DY3+nFJ/eiWatUit6',
'g.grey@gmail.com', 'MALE', 'the Grey');

INSERT INTO players (login, password, email, gender, fullname) /*password = 4444*/
VALUES ('Rose', 'WELhUsEODthmlubIvTkqeNH/k+DxTJxhWytcAIYkTfDAQA1GE4nszgGwmc02uGf/',
'rose.g@gmail.com', 'FEMALE', 'Rose Gamgee');