DROP TABLE IF EXISTS players;
DROP SEQUENCE IF EXISTS global_seq;

CREATE SEQUENCE global_seq START 1;

CREATE TABLE players
(
  id                BIGINT PRIMARY KEY DEFAULT nextval('global_seq'),
  login             VARCHAR UNIQUE NOT NULL,
  password          VARCHAR,
  email             VARCHAR,
  fullname          VARCHAR,
  gender            VARCHAR,
  birthday_date     DATE,
  registration_time TIMESTAMP,
  about             VARCHAR,
  avatar            VARCHAR
);

DELETE FROM players;
ALTER SEQUENCE global_seq RESTART WITH 1;

INSERT INTO players (login, password, email, gender, fullname) /*password = 1111*/
VALUES ('Sam', 'I4tRkyLjcOTNRwUAVfnIz+017/kKu+yLSEFGyo8nXstaQ0xLF99cXMkH0x0DWvhc',
        'gamgee@gmail.com', 'MALE', 'Gamgee');

INSERT INTO players (login, password, email, gender, fullname) /*password = 2222*/
VALUES ('Frodo', 'Oy+x64iYNRg865WOMh6RLeO56JKWmuYsHzwe2FfOkX/hoiTp3rAkWum/EtF2ntQ5',
        'shire.hole@gmail.com', 'MALE', 'Beggins');

INSERT INTO players (login, password, email, gender, fullname) /*password = 3333*/
VALUES ('Gandalf', 'gwXmufeTM7RVswMz0kZn649J1XjOcHsUDYWr+XFfSfGCX5+DY3+nFJ/eiWatUit6',
        'g.grey@gmail.com', 'MALE', 'the Grey');

INSERT INTO players (login, password, email, gender, fullname) /*password = 4444*/
VALUES ('Rose', 'WELhUsEODthmlubIvTkqeNH/k+DxTJxhWytcAIYkTfDAQA1GE4nszgGwmc02uGf/',
        'rose.g@gmail.com', 'FEMALE', 'Rose Gamgee');