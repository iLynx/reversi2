package com.softserveinc.ita.manygames.controllers;

import com.softserveinc.ita.manygames.model.Gender;
import com.softserveinc.ita.manygames.model.Player;
import com.softserveinc.ita.manygames.services.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Igor Khlaponin
 */

@Controller
@WebServlet("/playerslist")
public class PlayerListController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Autowired
    private PlayerService playerService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String currentPlayer = request.getParameter("name");

        request.setAttribute("currentPlayer", currentPlayer);
        request.setAttribute("playersList", playerService.getPlayers());

        request.getRequestDispatcher("/WEB-INF/views/playerslist.jsp").forward(request, response);
    }

}
