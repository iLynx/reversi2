package com.softserveinc.ita.manygames.controllers;

import com.softserveinc.ita.manygames.services.GameListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Igor Khlaponin
 */

@Controller
@WebServlet("/waitinggame/*")
public class WaitingGameController extends HttpServlet {

    @Autowired
    private GameListService gameListService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Long id = Long.parseLong(request.getParameter("id"));

        String opponent = gameListService.getGameById(id).getPlayer1().getName();

        request.setAttribute("id", id);
        request.setAttribute("name", request.getParameter("name"));
        request.setAttribute("opponent", opponent);

        request.getRequestDispatcher("/WEB-INF/views/waitinggame.jsp").forward(request, response);
    }
}
