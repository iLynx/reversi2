package com.softserveinc.ita.manygames.controllers;

import com.softserveinc.ita.manygames.model.Player;
import com.softserveinc.ita.manygames.services.GameListService;
import com.softserveinc.ita.manygames.services.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Igor Khlaponin
 */

@Controller
@WebServlet("/gameslist")
public class GamesListController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Autowired
    private PlayerService playerService;
    @Autowired
    private GameListService gameListService;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String playerName = (String) request.getSession().getAttribute("currentPlayer");

        Player player = playerService.getPlayerByName(playerName);

        List<Long> playingGamesList =  gameListService.getPlayingGamesIDs(player);
        List<Long> createdGamesList =  gameListService.getCreatedGamesIDs(player);
        List<Long> waitingGamesList =  gameListService.getWaitingGamesIDs(player);

        request.setAttribute("playingGames", playingGamesList);
        request.setAttribute("createdGames", createdGamesList);
        request.setAttribute("waitingGames", waitingGamesList);

        request.getRequestDispatcher("/WEB-INF/views/gameslist.jsp").forward(request, response);
    }
}
