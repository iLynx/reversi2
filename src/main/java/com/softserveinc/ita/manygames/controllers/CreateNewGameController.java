package com.softserveinc.ita.manygames.controllers;

import com.softserveinc.ita.manygames.services.GameListService;
import com.softserveinc.ita.manygames.services.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Igor Khlaponin
 */

@Controller
@WebServlet("/creategame")
public class CreateNewGameController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Autowired
    private PlayerService playerService;
    @Autowired
    private GameListService gameListService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String currentPlayerName = request.getParameter("name");

        gameListService.createGame(playerService.getPlayerByName(currentPlayerName));

        request.setAttribute("currentPlayer", currentPlayerName);

        request.getRequestDispatcher("/gameslist").forward(request, response);
    }
}
