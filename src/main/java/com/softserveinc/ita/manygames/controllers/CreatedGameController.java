package com.softserveinc.ita.manygames.controllers;

import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Igor Khlaponin
 */

@Controller
@WebServlet("/createdgame/*")
public class CreatedGameController extends HttpServlet{

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("id", request.getParameter("id"));
        request.setAttribute("name", request.getParameter("name"));

        request.getRequestDispatcher("/WEB-INF/views/createdgame.jsp").forward(request, response);
    }
}
