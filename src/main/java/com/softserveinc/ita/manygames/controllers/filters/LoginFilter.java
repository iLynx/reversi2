package com.softserveinc.ita.manygames.controllers.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author Igor Khlaponin
 */

@WebFilter(filterName = "LoginFilter", urlPatterns = {"/*"})
public class LoginFilter implements Filter {

    private HttpServletRequest httpRequest;
    private HttpServletResponse httpResponse;
    private HttpSession session;

    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        httpRequest = (HttpServletRequest) request;
        httpResponse = (HttpServletResponse) response;
        session = httpRequest.getSession(false);

        String loginURI = httpRequest.getContextPath() + "/login";
        String logoutURI = httpRequest.getContextPath() + "/logout";
        String registrationURI = httpRequest.getContextPath() + "/registration";
        String resourcesURI = httpRequest.getContextPath() + "/img";

        boolean isLogin = (session != null) && (session.getAttribute("currentPlayer") != null);
        boolean isLoginRequest = httpRequest.getRequestURI().startsWith(loginURI);
        boolean isLogoutRequest = httpRequest.getRequestURI().startsWith(logoutURI);
        boolean isRegisterRequest = httpRequest.getRequestURI().startsWith(registrationURI);
        boolean isResourcesRequest = httpRequest.getRequestURI().startsWith(resourcesURI);

        if (isLogin && (isLoginRequest || isRegisterRequest)) {
            httpResponse.sendRedirect("gameslist");
        } else if (isLogin || isLoginRequest || isLogoutRequest || isRegisterRequest || isResourcesRequest) {
            chain.doFilter(request, response);
        } else {
            httpRequest.getRequestDispatcher("index.jsp").forward(request, response);
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
