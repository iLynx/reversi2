package com.softserveinc.ita.manygames.controllers;

import com.softserveinc.ita.manygames.model.Player;
import com.softserveinc.ita.manygames.services.PlayerService;
import com.softserveinc.ita.manygames.utils.PasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author Igor Khlaponin
 */

@Controller
@WebServlet("/login")
public class LoginController extends HttpServlet {

    @Autowired
    private PlayerService playerService;

    private Player player;

    public void setPlayer(Player player) {
        this.player = player;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String login = request.getParameter("name");
        String password = request.getParameter("password");

        if (login == null || login.equals("") || password == null || password.equals("")) {
            request.setAttribute("message", "Wrong user or password! Please, try again");
            request.getRequestDispatcher("/index.jsp")
                    .forward(request, response);
        }

        player = playerService.getPlayerByName(login);

        if (player == null) {
            request.getRequestDispatcher("/WEB-INF/views/registration.jsp")
                    .forward(request, response);
        } if (!PasswordEncryptor.checkPassword(password, player)){
            request.setAttribute("message", "Wrong user or password! Please, try again");
            request.getRequestDispatcher("/index.jsp")
                    .forward(request, response);
        } else {
            HttpSession session = request.getSession();
            session.setMaxInactiveInterval(0); //indicates that the session should never timeout
            session.setAttribute("currentPlayer", login);
            response.sendRedirect("gameslist");
        }

    }

}
