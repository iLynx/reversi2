package com.softserveinc.ita.manygames.controllers;

import com.softserveinc.ita.manygames.model.TurnLog;
import com.softserveinc.ita.manygames.services.GameListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Igor Khlaponin
 */

@Controller
@WebServlet("/updatefield")
public class UpdateFieldController extends HttpServlet {

    @Autowired
    private GameListService gameListService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Long id = Long.parseLong(request.getParameter("id"));

        response.getWriter().write(gameListService.getGameById(id).getEngine().getBoard());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String turn = request.getParameter("turn");
        Long id = Long.parseLong(request.getParameter("id"));
        String currentPlayer = request.getParameter("currentPlayer");

        if (gameListService.getGameById(id).getEngine().makeTurn(currentPlayer, turn)){
            gameListService.getGameById(id).addTurn(new TurnLog(currentPlayer, turn));
        };

        response.getWriter().write(gameListService.getGameById(id).getEngine().getBoard());
    }

}
