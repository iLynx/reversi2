package com.softserveinc.ita.manygames.controllers;

import com.softserveinc.ita.manygames.model.*;
import com.softserveinc.ita.manygames.services.GameHistoryService;
import com.softserveinc.ita.manygames.services.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Igor Khlaponin
 */

@Controller
@WebServlet("/registration")
public class RegistrationRedirectController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private GameHistoryService gameHistoryService;


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/views/registration.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String newPlayerName = request.getParameter("name");
        String password = request.getParameter("password");
        if (newPlayerName == null || newPlayerName.equals("") || password == null) {
            request.getRequestDispatcher("/WEB-INF/views/registration.jsp").forward(request, response);
        }
        String email = request.getParameter("email");
        String newPlayerFullName = request.getParameter("fullName");
        Gender gender = (request.getParameter("gender") == null)
                ? null
                : Gender.valueOf(request.getParameter("gender"));
        LocalDate birthdayDate = (request.getParameter("birthdayDate") == null
                || request.getParameter("birthdayDate").equals(""))
                ? null
                : LocalDate.parse(request.getParameter("birthdayDate"));
        LocalDateTime registrationTime = LocalDateTime.now();
        String about = request.getParameter("about");

        Player newPlayer = new Player();
        newPlayer.setName(newPlayerName); //login
        newPlayer.addPassword(password);
        newPlayer.setEmail(email);
        newPlayer.setFullName(newPlayerFullName);
        newPlayer.setGender(gender);
        newPlayer.setBirthdayDate(birthdayDate);
        newPlayer.setRegistrationTime(registrationTime);
        newPlayer.setAbout(about);
        playerService.registerNewPlayer(newPlayer);

        HttpSession session = request.getSession();
        session.setAttribute("currentPlayer", newPlayer.getName());

        request.setAttribute("currentPlayer", newPlayer.getName());
        request.setAttribute("playersList", playerService.getPlayers());

        doInit(newPlayer);

        request.getRequestDispatcher("/WEB-INF/views/playerslist.jsp").forward(request, response);

    }

    public void doInit(Player player1) { //TODO only for demo. Should be deleted from production
        Player player2 = new Player("Frodo", "2222");
        player2.setGender(Gender.MALE);
        Player player3 = new Player("Gandalf", "3333");
        player3.setGender(Gender.MALE);

        GameHistory gameHistory = new GameHistory();
        gameHistory.setId(10L);
        gameHistory.setPlayer1(player2);
        gameHistory.setPlayer2(player1);
        gameHistory.setStartTime(LocalDateTime.now());
        gameHistory.setResultStatus(ResultStatus.FIRST_IS_A_WINNER);

        List<String> turnsList = new ArrayList<>();
        turnsList.add((new TurnLog(player2.getName(), "f4")).toString());
        turnsList.add((new TurnLog(player1.getName(), "d3")).toString());
        turnsList.add((new TurnLog(player2.getName(), "c4")).toString());
        turnsList.add((new TurnLog(player1.getName(), "f5")).toString());
        gameHistory.setTurnList(turnsList);

        gameHistory.setEndTime(LocalDateTime.now());

        GameHistory gameHistory2 = new GameHistory();
        gameHistory2.setId(11L);
        gameHistory2.setPlayer1(player1);
        gameHistory2.setPlayer2(player3);
        gameHistory2.setStartTime(LocalDateTime.now());
        gameHistory2.setEndTime(LocalDateTime.now());
        gameHistory2.setResultStatus(ResultStatus.SECOND_IS_A_WINNER);

        List<String> turns2 = new ArrayList<>();
        turns2.add((new TurnLog(player1.getName(), "f4")).toString());
        turns2.add((new TurnLog(player3.getName(), "d3")).toString());
        turns2.add((new TurnLog(player1.getName(), "c4")).toString());
        gameHistory2.setTurnList(turns2);

        gameHistoryService.createNewGameHistory(gameHistory);
        gameHistoryService.createNewGameHistory(gameHistory2);

    }
}
