package com.softserveinc.ita.manygames.controllers;

import com.softserveinc.ita.manygames.services.GameListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Igor Khlaponin
 */

@Controller
@WebServlet("/refreshcurrentgame")
public class CurrentGameRefreshController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Autowired
    private GameListService gameListService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String currentPlayer = request.getParameter("name");
        Long gameNumber = Long.parseLong(request.getParameter("id"));

        request.setAttribute("currentPlayer", currentPlayer);

        String color = (gameListService.getGameById(gameNumber)).getPlayer1().getName().equals(currentPlayer)
                ? "white"
                : "black";
        request.setAttribute("pawnColor", color);
        request.setAttribute("matrix", gameListService.getGameById(gameNumber).getEngine().getBoard());
        String opponent = gameListService.getGameById(gameNumber).getPlayer1().getName().equals(currentPlayer)
                ? gameListService.getGameById(gameNumber).getPlayer2().getName()
                : gameListService.getGameById(gameNumber).getPlayer1().getName();
        request.setAttribute("opponent", opponent);
        request.setAttribute("gameNumber", gameNumber);
        request.setAttribute("resultCode", gameListService.getGameById(gameNumber).getEngine().getResultCode());

        request.getRequestDispatcher("/WEB-INF/views/currentgame.jsp").forward(request, response);

    }

}

