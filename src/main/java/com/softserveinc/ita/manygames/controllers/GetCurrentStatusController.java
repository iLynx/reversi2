package com.softserveinc.ita.manygames.controllers;

import com.softserveinc.ita.manygames.model.Game;
import com.softserveinc.ita.manygames.model.Player;
import com.softserveinc.ita.manygames.services.GameHistoryService;
import com.softserveinc.ita.manygames.services.GameListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Igor Khlaponin
 */

@Controller
@WebServlet(name = "GetCurrentStatusController", urlPatterns = "/getCurrentStatus")
public class GetCurrentStatusController extends HttpServlet {

    @Autowired
    private GameListService gameListService;
    @Autowired
    private GameHistoryService gameHistoryService;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long id = Long.valueOf(request.getParameter("gameId"));
        Game currentGame = gameListService.getGameById(id);
        Player firstPlayer = currentGame.getPlayer1();
        Player secondPlayer = currentGame.getPlayer2();

        int gameState = currentGame.getEngine().getGameState();
        String gameStateToShow = "";
        if (gameState == 3) {
            gameStateToShow = "Please, wait while " + firstPlayer.getName() + " makes turn... ";
        } else if (gameState == 4) {
            gameStateToShow = "Please, wait while " + secondPlayer.getName() + " makes turn... ";
        } else if (gameState == 5) {
            gameStateToShow = "<h2 style='color: red;'>" + firstPlayer.getName() + "is a winner!</h2>";
            gameHistoryService.createNewGameHistory(currentGame);
            gameListService.deleteGame(id);
        } else if (gameState == 6) {
            gameStateToShow = "<h2 style='color: red;'>" + secondPlayer.getName() + "is a winner!</h2>";
            gameHistoryService.createNewGameHistory(currentGame);
            gameListService.deleteGame(id);
        } else if (gameState == 7) {
            gameStateToShow = "<h2 style='color: red;'>Draw!</h2>";
            gameHistoryService.createNewGameHistory(currentGame);
            gameListService.deleteGame(id);
        }


        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(gameStateToShow);
    }
}
