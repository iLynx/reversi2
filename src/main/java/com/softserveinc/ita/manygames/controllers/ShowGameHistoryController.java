package com.softserveinc.ita.manygames.controllers;

import com.softserveinc.ita.manygames.model.GameHistory;
import com.softserveinc.ita.manygames.model.Player;
import com.softserveinc.ita.manygames.model.ResultStatus;
import com.softserveinc.ita.manygames.model.TurnLog;
import com.softserveinc.ita.manygames.services.GameHistoryService;
import com.softserveinc.ita.manygames.services.PlayerService;
import com.softserveinc.ita.manygames.utils.JsonBuilder;
import jdk.nashorn.internal.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Igor Khlaponin
 */

@Controller
@WebServlet("/gamehistory/*")
public class ShowGameHistoryController extends HttpServlet {

    @Autowired
    private GameHistoryService gameHistoryService;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Long id = Long.parseLong(request.getParameter("id"));
        GameHistory gameHistory = gameHistoryService.getGameHistory(id);
        List<TurnLog> turns = new ArrayList<>();
        for (String str : gameHistory.getTurnList()) {
            turns.add((TurnLog) JsonBuilder.fromJson(str, TurnLog.class));
        }
        request.setAttribute("id", id);
        request.setAttribute("player1", gameHistory.getPlayer1());
        request.setAttribute("player2", gameHistory.getPlayer2());
        request.setAttribute("startTime", gameHistory.getStartTime());
        request.setAttribute("endTime", gameHistory.getEndTime());
        request.setAttribute("result", gameHistory.getResultStatus());
        request.setAttribute("turnList", turns);
        /*request.setAttribute("gameHistory", JsonBuilder.toJsonStringify(gameHistory))*/;

        request.getRequestDispatcher("/WEB-INF/views/gamehistory.jsp").forward(request, response);
    }

}
