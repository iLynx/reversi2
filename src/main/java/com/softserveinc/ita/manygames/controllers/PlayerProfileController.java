package com.softserveinc.ita.manygames.controllers;

import com.softserveinc.ita.manygames.model.GameHistory;
import com.softserveinc.ita.manygames.model.Player;
import com.softserveinc.ita.manygames.services.GameHistoryService;
import com.softserveinc.ita.manygames.services.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Igor Khlaponin
 */

@Controller
@WebServlet("/player/*")
public class PlayerProfileController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private GameHistoryService gameHistoryService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String playerName = request.getParameter("playername");
        String currentPlayer = request.getParameter("currentplayer");

        Player player = playerService.getPlayerByName(playerName);
        List<GameHistory> gameHistories = gameHistoryService.getAllGamesHistory(player);

        request.setAttribute("playerName", player.getName());
        request.setAttribute("currentPlayer", currentPlayer);
        request.setAttribute("player", player.toString());
        request.setAttribute("gameHistories", gameHistories);

        request.getRequestDispatcher("/WEB-INF/views/profile.jsp").forward(request, response);
    }
}
