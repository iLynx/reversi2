package com.softserveinc.ita.manygames.controllers;

import com.softserveinc.ita.manygames.services.GameListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Igor Khlaponin
 */

@Controller
@WebServlet("/getResultCode")
public class GetResultCodeController extends HttpServlet {

    @Autowired
    private GameListService gameListService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Long id = Long.parseLong(request.getParameter("id"));

        response.getWriter().write("" + gameListService.getGameById(id).getEngine().getResultCode());
    }
}
