package com.softserveinc.ita.manygames.utils;

import com.softserveinc.ita.manygames.model.Player;
import org.jasypt.util.password.StrongPasswordEncryptor;

/**
 * @author Igor Khlaponin
 */
public class PasswordEncryptor {

    public static String getEncryptedPassword(String originalPassword) {
        StrongPasswordEncryptor passwordEncryptorStrong = new StrongPasswordEncryptor();
        return passwordEncryptorStrong.encryptPassword(originalPassword);
    }

    public static boolean checkPassword(String password, Player player) {
        StrongPasswordEncryptor passwordEncryptorStrong = new StrongPasswordEncryptor();
        return passwordEncryptorStrong.checkPassword(password, player.getPassword());
    }

}
