package com.softserveinc.ita.manygames.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.time.LocalDateTime;

/**
 * @author Igor Khlaponin
 */

public class JsonBuilder {

    public static String toJson(Object object) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(object);
    }

    public static String toJsonStringify(Object object) {
        Gson gson = new Gson();
        return gson.toJson(object);
    }

    public static Object fromJson(String json, Class clazz) {
        Gson gson = new Gson();
        return gson.fromJson(json, clazz);
    }
}
