package com.softserveinc.ita.manygames.dao;

import com.softserveinc.ita.manygames.model.Player;

import java.util.List;

/**
 * @author Igor Khlaponin
 */
public interface PlayerListDao {
    boolean registerNewPlayer(Player player);
    boolean contains(Player player);
    Player getPlayerByName(String playerName);
    List<Player> getAllPlayers();
}
