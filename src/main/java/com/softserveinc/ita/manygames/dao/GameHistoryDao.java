package com.softserveinc.ita.manygames.dao;

import com.softserveinc.ita.manygames.model.Game;
import com.softserveinc.ita.manygames.model.GameHistory;
import com.softserveinc.ita.manygames.model.Player;

import java.util.List;

/**
 * @author Igor Khlaponin
 */
public interface GameHistoryDao {

    void createNewGameHistory(Game game);
    void createNewGameHistory(GameHistory gameHistory);
    void update(GameHistory gameHistory);
    void delete(Long id);
    GameHistory getGameHistory(Long gameId);
    List<GameHistory> getAllGamesHistory(Player player);
}
