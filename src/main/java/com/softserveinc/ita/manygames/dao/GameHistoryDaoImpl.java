package com.softserveinc.ita.manygames.dao;

import com.softserveinc.ita.manygames.model.*;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Igor Khlaponin
 */

@Repository
public class GameHistoryDaoImpl implements GameHistoryDao {

    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger log = Logger.getLogger(GameHistoryDaoImpl.class);

    @Override
    public void createNewGameHistory(Game game) {
        GameHistory gameHistory = new GameHistory(game);
        try {
            sessionFactory.getCurrentSession().save(gameHistory);
            log.info("created game history #" + game.getId());
        } catch (Exception e) {
            log.error("game history isn't created!");
        }
    }

    @Override
    public void createNewGameHistory(GameHistory gameHistory) {
        try {
            sessionFactory.getCurrentSession().save(gameHistory);
            log.info("created game history #" + gameHistory.getId());
        } catch (Exception e) {
            log.error("game history isn't created!");
        }
    }

    @Override
    public void update(GameHistory gameHistory) {
        try {
            sessionFactory.getCurrentSession().update(gameHistory);
            log.info("updated game history #" + gameHistory.getId());
        } catch (Exception e) {
            log.error("game history isn't updated!");
        }
    }

    @Override
    public void delete(Long id) {
        try {
            Criteria criteria = sessionFactory.getCurrentSession().createCriteria(GameHistory.class);
            criteria.add(Restrictions.eq("id", id));
            GameHistory gameHistory = (GameHistory) criteria.uniqueResult();
            sessionFactory.getCurrentSession().delete(gameHistory);
            log.info("game history #" + id + " have been deleted");
        } catch (Exception e) {
            log.error("game history haven't been deleted!");
        }
    }

    @Override
    public GameHistory getGameHistory(Long gameId) {
        if (gameId == null) {
            return null;
        }
        GameHistory gameHistory = sessionFactory.getCurrentSession().get(GameHistory.class, gameId);
        gameHistory.getTurnList().isEmpty();
        log.info("game history #" + gameId + " have got from storage");
        return gameHistory;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<GameHistory> getAllGamesHistory(Player player) {
        if (player == null) {
            return null;
        }
        List<GameHistory> histories;
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(GameHistory.class);
        Criterion player1Matches = Restrictions.eq("player1", player);
        Criterion player2Matches = Restrictions.eq("player2", player);
        LogicalExpression orExp = Restrictions.or(player1Matches, player2Matches);
        criteria.add(orExp);
        histories = criteria.list();
        log.info("All games histories for " + player.getName() + " have got from storage");
        return histories;
    }


}
