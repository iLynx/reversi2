package com.softserveinc.ita.manygames.dao;

import com.softserveinc.ita.manygames.model.Player;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Igor Khlaponin
 */

@Repository
public class PlayerListDaoHibernateImpl implements PlayerListDao {

    @Autowired
    private SessionFactory sessionFactory;
    private static final Logger log = Logger.getLogger(PlayerListDaoHibernateImpl.class);

    @Override
    public boolean registerNewPlayer(Player player) {
        try {
            sessionFactory.getCurrentSession().save(player);
            log.info("player " + player.getName() + " registered");
            return true;
        } catch (Exception e) {
            log.error("player hasn't been registered");
            return false;
        }
    }

    @Override
    public boolean contains(Player player) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Player.class);
        criteria.add(Restrictions.eq("name", player.getName()));
        Player player1 = (Player) criteria.uniqueResult();
        return player1 != null;
    }

    @Override
    public Player getPlayerByName(String playerName) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Player.class);
        criteria.add(Restrictions.eq("name", playerName));
        Player player = (Player) criteria.uniqueResult();
        log.info("player " + playerName + " has got from storage");
        return player;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Player> getAllPlayers() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Player.class);
        List<Player> players = criteria.list();
        log.info("All players have got from storage");
        return players;
    }
}
