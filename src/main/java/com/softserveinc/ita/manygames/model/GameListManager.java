package com.softserveinc.ita.manygames.model;

import java.util.List;

/**
 * @author Igor Khlaponin
 */
public interface GameListManager {
    boolean createGame(Player player);
    boolean deleteGame(Long id);
    List<Long> getCreatedGamesIDs(Player player);
    List<Long> getPlayingGamesIDs(Player player);
    List<Long> getWaitingGamesIDs(Player player);
    Game getGameById(Long id);
}
