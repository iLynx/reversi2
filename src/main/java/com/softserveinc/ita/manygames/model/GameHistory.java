package com.softserveinc.ita.manygames.model;

import com.softserveinc.ita.manygames.model.engine.GameState;
import com.softserveinc.ita.manygames.model.engine.Reversi.Reversi;
import com.softserveinc.ita.manygames.utils.JsonBuilder;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Igor Khlaponin
 */

@Entity
@Table(name = "games_history", uniqueConstraints = @UniqueConstraint(columnNames = "game_id"))
public class GameHistory implements Serializable {

    private Long id;
    private Player player1;
    private Player player2;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private List<String> turnList;
    private ResultStatus resultStatus;

    public GameHistory() {}

    public GameHistory(Long id, Player player1, Player player2,
                       LocalDateTime startTime, LocalDateTime endTime,
                       List<String> turnList, ResultStatus resultStatus) {
        this.id = id;
        this.player1 = player1;
        this.player2 = player2;
        this.startTime = startTime;
        this.endTime = endTime;
        this.turnList = turnList;
        this.resultStatus = resultStatus;
    }

    public GameHistory(Game game) {
        this.id = game.getId();
        this.player1 = game.getPlayer1();
        this.player2 = game.getPlayer2();
        this.startTime = game.getStartTime();
        this.endTime = LocalDateTime.now();
        this.turnList = game.getTurnList();
        this.resultStatus = transformResultStatus(game.getEngine().getGameState());
    }

    public static ResultStatus transformResultStatus(int resultCode) {
        ResultStatus resultStatus1 = null;
        switch (resultCode){
            case GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER: {
                resultStatus1 = ResultStatus.FIRST_IS_A_WINNER;
                break;
            }
            case GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER: {
                resultStatus1 = ResultStatus.SECOND_IS_A_WINNER;
                break;
            }
            default: {
                resultStatus1 = ResultStatus.DRAWN;
            }
        }
        return resultStatus1;
    }

    @Id
    @Column(name = "game_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "first_player_id", nullable = false)
    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "second_player_id", nullable = false)
    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    @NotNull
    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    @NotNull
    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "turn_list",
            joinColumns = @JoinColumn(name = "game_id", referencedColumnName = "game_id"))
    @NotNull
    @Column(name="turn")
    public List<String> getTurnList() {
        return turnList;
    }

    public void setTurnList(List<String> turnList) {
        this.turnList = turnList;
    }

    @Enumerated
    @NotNull
    public ResultStatus getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(ResultStatus resultStatus) {
        this.resultStatus = resultStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameHistory that = (GameHistory) o;

        if (!id.equals(that.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return String.format("Game #%d [player1: %s], [player2: %s]. Result: %s",
                id, player1.getName(), player2.getName(), resultStatus);
    }
}
