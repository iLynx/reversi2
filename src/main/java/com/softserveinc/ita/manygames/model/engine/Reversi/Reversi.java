package com.softserveinc.ita.manygames.model.engine.Reversi;

import com.softserveinc.ita.manygames.utils.JsonBuilder;
import com.softserveinc.ita.manygames.model.engine.GameState;
import com.softserveinc.ita.manygames.model.engine.GenericGameEngine;


/**
 * @author Igor Khlaponin
 */
public class Reversi extends GenericGameEngine<String> {

    private Board board = new Board();

    public Reversi(){
        super();
    }

    public Reversi(Long id) {
        super(id);
    }

    public Board getGameBoard(){
        return this.board;
    }

    @Override
    public String getBoard() {
        return JsonBuilder.toJsonStringify(ReversiUtil.arrayToCellsListParser(board.getField()));
    }

    @Override
    protected boolean validateTurnSyntax(String turn) {
        if (turn == null) return false;
        if (turn.length() != 2) return false;

        char charX = turn.charAt(0); //it should be letter from a to h (or from A to H)
        char charY = turn.charAt(turn.length()-1); //it should be digit from 1 to 8
        if (!(charX >= 65 && charX <= 72) && !(charX >= 97 && charX <= 104)) return false;
        if (!(charY >= 49 && charY <= 56)) return false;

        return true;
    }

    @Override
    protected boolean validateTurnLogic(String turn) {
        Turn currentTurn = new Turn(turn);
    	int currentCellX = currentTurn.getKey();
    	int currentCellY = currentTurn.getValue();
    	int[][] field = board.getField();
    	//check if we turn in the empty cell which is round with empty cells
    	if (!cellHasNeighbours(currentCellX, currentCellY)) {
            return false;
        }
        //check if we turn in the busy cell
        if (field[currentCellY][currentCellX] != 0) {
            return false;
        }
        return true;
    }

    @Override
    protected boolean validatePlayer(String playerName) {
        if (playerName == null) return false;
        return !playerName.equals("");
    }

    @Override
    protected int changeGameState(String playerName, String turn) {
        int expectedGameState = gameState;
        //put the pawn on the board according to the player turn
        board.putPawn(turn, gameState);

        //check if someone won
        //set the winner
        if (!board.hasMoreTurns()){
            if (board.getWinnerColor() == Color.WHITE) {
                this.setTheWinner(this.getFirstPlayer());
                return GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
            } else if (board.getWinnerColor() == Color.BLACK) {
                this.setTheWinner(this.getSecondPlayer());
                return GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
            }
        }

        //change state if the game isn't finished
        if (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN) {
            expectedGameState = GameState.WAIT_FOR_SECOND_PLAYER_TURN;
        } else if (gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN){
            expectedGameState = GameState.WAIT_FOR_FIRST_PLAYER_TURN;
        }

        return expectedGameState;
    }

    protected String getCurrentPlayer(){
        String currentPlayerName = "";
        if (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN){
            currentPlayerName = this.getFirstPlayer();
        } else if (gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN){
            currentPlayerName = this.getSecondPlayer();
        }
        return currentPlayerName;
    }

    public int getCurrentGameState() {
        return gameState;
    }

    private boolean cellHasNeighbours(int x, int y){
        int[][] field = board.getField();
        return hasHorizontalNeighbour(x, y, field)
                || hasVerticalNeighbours(x, y, field)
                || hasDiagonalNeighbour(x, y, field);
    }

    private boolean hasHorizontalNeighbour(int x, int y, int[][] field) {
        int max = field.length - 1;
        int plusDeltaX = (x == max) ? 0 : 1;
        int minusDeltaX = (x == 0) ? 0 : 1;
        return (field[y][x + plusDeltaX] != Color.EMPTY_CELL)
                || (field[y][x - minusDeltaX] != Color.EMPTY_CELL);
    }

    private boolean hasVerticalNeighbours(int x, int y, int[][] field) {
        int max = field.length - 1;
        int plusDeltaY = (y == max) ? 0 : 1;
        int minusDeltaY = (y == 0) ? 0 : 1;
        return (field[y - minusDeltaY][x] != Color.EMPTY_CELL)
                || (field[y + plusDeltaY][x] != Color.EMPTY_CELL);
    }

    private boolean hasDiagonalNeighbour(int x, int y, int[][] field){
        int max = field.length - 1;
        int plusDeltaX = (x == max) ? 0 : 1;
        int minusDeltaX = (x == 0) ? 0 : 1;
        int plusDeltaY = (y == max) ? 0 : 1;
        int minusDeltaY = (y == 0) ? 0 : 1;
        return (field[y - minusDeltaY][x + plusDeltaX] != Color.EMPTY_CELL)
                || (field[y- minusDeltaY][x - minusDeltaX] != Color.EMPTY_CELL)
                || (field[y + plusDeltaY][x + plusDeltaX] != Color.EMPTY_CELL)
                || (field[y + plusDeltaY][x - minusDeltaX] != Color.EMPTY_CELL);
    }

}
