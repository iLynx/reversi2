package com.softserveinc.ita.manygames.model;

/**
 * @author Igor Khlaponin
 */

public enum ResultStatus {
    FIRST_IS_A_WINNER,
    SECOND_IS_A_WINNER,
    DRAWN
}
