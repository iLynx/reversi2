package com.softserveinc.ita.manygames.model;

import com.softserveinc.ita.manygames.model.engine.GenericGameEngine;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Igor Khlaponin
 */
public class Game {

    private GenericGameEngine<String> engine;
    private Player player1;
    private Player player2;
    private Long id;
    private LocalDateTime startTime;
    private List<String> turnList = new ArrayList<>();

    public Game(Player player1, GenericGameEngine<String> engine) {
        this.player1 = player1;
        this.engine = engine;
        engine.setFirstPlayer(player1.getName());
        this.id = engine.getId();
        this.startTime = LocalDateTime.now();
    }

    public GenericGameEngine<String> getEngine() {
        return engine;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        if (this.player1 == null) {
            this.player1 = player1;
        }
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        if (this.player2 == null) {
            this.player2 = player2;
            engine.setSecondPlayer(player2.getName());
        }
    }

    public Long getId() {
        return id;
    }

    public List<String> getTurnList() {
        return turnList;
    }

    public void setTurnList(List<String> turnList) {
        this.turnList = turnList;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public void addTurn(TurnLog turn) {
        turnList.add(turn.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Game game = (Game) o;

        if (!engine.equals(game.engine)) return false;

        return true;
    }

    public boolean isFinished() {
        return this.engine.isFinished();
    }

    @Override
    public int hashCode() {
        return engine.hashCode();
    }

    @Override
    public String toString() {
        return "Game #" + this.getId();
    }

}
