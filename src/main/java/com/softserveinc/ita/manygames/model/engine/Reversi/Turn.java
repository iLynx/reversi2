package com.softserveinc.ita.manygames.model.engine.Reversi;

import java.util.Map;

/**
 * @author Igor Khlaponin
 */
public class Turn {

    private Integer key;
    private Integer value;

    public Turn(String turn){
        for (Map.Entry<Integer, Integer> pair : ReversiUtil.turnParser(turn).entrySet()) {
            key = pair.getKey();
            value = pair.getValue();
        }
        if (key == null){
            key = -1;
        }
        if (value == null) {
            value = -1;
        }
    }

    public int getKey() {
        return key;
    }

    public int getValue() {
        return value;
    }
}
