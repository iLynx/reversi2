package com.softserveinc.ita.manygames.model;

/**
 * @author Igor Khlaponin
 */
public enum Gender {
    MALE,
    FEMALE
}
