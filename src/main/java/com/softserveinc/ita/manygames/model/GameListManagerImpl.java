package com.softserveinc.ita.manygames.model;

import com.softserveinc.ita.manygames.dao.PlayerListDao;
import com.softserveinc.ita.manygames.model.engine.Reversi.Reversi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Igor Khlaponin
 */

@Component
public class GameListManagerImpl implements GameListManager {

    private static Map<Long, Game> map;

    private static GameListManagerImpl instance = null;

    @Autowired
    private PlayerListDao playerListDao;

    private GameListManagerImpl() {
        map = new HashMap<Long, Game>();
        init();
    }

    public static GameListManagerImpl getInstance() {
        if (instance == null) {
            instance = new GameListManagerImpl();
        }
        return instance;
    }

    private void init() {
        createGame(playerListDao.getPlayerByName("Frodo"));
        createGame(playerListDao.getPlayerByName("Sam"));
        createGame(playerListDao.getPlayerByName("Gandalf"));
    }

    @Override
    public boolean createGame(Player player) {
        if (player == null) {
            return false;
        }
        Reversi reversi = new Reversi();

        reversi.setFirstPlayer(player.getName());
        Game game = new Game(player, reversi);
        map.put(game.getEngine().getId(), game);

        return map.containsKey(game.getEngine().getId());
    }

    @Override
    public boolean deleteGame(Long id) {
        map.remove(id);
        return true;
    }

    @Override
    public List<Long> getCreatedGamesIDs(Player player) {
        List<Long> list = new ArrayList<Long>();
        for (Map.Entry<Long, Game> pair : map.entrySet() ) {
            if (pair.getValue().getPlayer1().equals(player)
                    && pair.getValue().getPlayer2() == null) {
                list.add(pair.getKey());
            }
        }
        return list;
    }

    @Override
    public List<Long> getPlayingGamesIDs(Player player) {
        List<Long> list = new ArrayList<Long>();
        for (Map.Entry<Long, Game> pair : map.entrySet() ) {
            if (pair.getValue().getPlayer2() != null &&
                    (pair.getValue().getPlayer1().equals(player) ||
                    pair.getValue().getPlayer2().equals(player))) {
                list.add(pair.getKey());
            }
        }
        return list;
    }

    @Override
    public List<Long> getWaitingGamesIDs(Player player) {
        List<Long> list = new ArrayList<Long>();
        for (Map.Entry<Long, Game> pair : map.entrySet() ) {
            if (!pair.getValue().getPlayer1().equals(player)
                    && pair.getValue().getPlayer2() == null) {
                list.add(pair.getKey());
            }
        }
        return list;
    }

    @Override
    public Game getGameById(Long id) {
        return map.get(id);
    }
}
