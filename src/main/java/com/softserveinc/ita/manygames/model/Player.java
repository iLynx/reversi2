package com.softserveinc.ita.manygames.model;

import com.softserveinc.ita.manygames.utils.PasswordEncryptor;
import com.softserveinc.ita.manygames.utils.JsonBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Igor Khlaponin
 */

@Entity(name = "player")
@Table(name = "players2")
public class Player implements Serializable {

    private Long id;
    private String name; //login
    private String password; //encrypted
    private String email;
    private String fullName;
    private Gender gender;
    private LocalDate birthdayDate;
    private LocalDateTime registrationTime;
    private String about;
    private String avatar; // keep like a path to DB

    public Player() {
    }

    public Player(Long id) {
        this.id = id;
    }

    public Player(String login, String password) {
        this.name = login;
        this.password = PasswordEncryptor.getEncryptedPassword(password);
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "login", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "password", nullable = false)
    public String getPassword() {
        return password;
    }

    public void addPassword(String password) {
        this.password = PasswordEncryptor.getEncryptedPassword(password);
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Enumerated(EnumType.STRING)
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(LocalDate birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    public LocalDateTime getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(LocalDateTime registrationTime) {
        this.registrationTime = registrationTime;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        if (!name.equals(player.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return JsonBuilder.toJsonStringify(this);
    }

}
