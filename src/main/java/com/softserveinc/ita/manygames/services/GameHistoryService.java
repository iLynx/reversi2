package com.softserveinc.ita.manygames.services;

import com.softserveinc.ita.manygames.dao.GameHistoryDao;
import com.softserveinc.ita.manygames.model.Game;
import com.softserveinc.ita.manygames.model.GameHistory;
import com.softserveinc.ita.manygames.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Igor Khlaponin
 */

@Service
public class GameHistoryService {

    @Autowired
    private GameHistoryDao gameHistoryDao;


    @Transactional
    public void createNewGameHistory(Game game) {
        gameHistoryDao.createNewGameHistory(game);
    }

    @Transactional
    public void createNewGameHistory(GameHistory gameHistory) {
        gameHistoryDao.createNewGameHistory(gameHistory);
    }

    @Transactional
    public void update(GameHistory gameHistory) {
        gameHistoryDao.update(gameHistory);
    }

    @Transactional
    public void delete(Long id) {
        gameHistoryDao.delete(id);
    }

    @Transactional(readOnly = true)
    public GameHistory getGameHistory(Long gameId) {
        return gameHistoryDao.getGameHistory(gameId);
    }

    @Transactional(readOnly = true)
    public List<GameHistory> getAllGamesHistory(Player player) {
        return gameHistoryDao.getAllGamesHistory(player);
    }
}
