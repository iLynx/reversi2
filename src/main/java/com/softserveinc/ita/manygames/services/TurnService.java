package com.softserveinc.ita.manygames.services;

import com.softserveinc.ita.manygames.model.GameListManager;
import com.softserveinc.ita.manygames.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Igor Khlaponin
 */

@Service
public class TurnService {

    @Autowired
    private GameListManager gameListManager;

    public void setGameListManager(GameListManager gameListManager) {
        this.gameListManager = gameListManager;
    }

    public boolean makeTurn(Player player, Long id, String turn) {
        return gameListManager.getGameById(id).getEngine().makeTurn(player.getName(), turn);
    }

    public String getGameField(Long gameId) {
        return gameListManager.getGameById(gameId).getEngine().getBoard();
    }

}
