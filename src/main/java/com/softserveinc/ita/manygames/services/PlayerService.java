package com.softserveinc.ita.manygames.services;

import com.softserveinc.ita.manygames.model.Player;
import com.softserveinc.ita.manygames.dao.PlayerListDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Igor Khlaponin
 */

@Service
public class PlayerService {

    @Autowired
    private PlayerListDao playerListDao;

    @Transactional
    public void setPlayerListDao(PlayerListDao playerListDao) {
        this.playerListDao = playerListDao;
    }

    @Transactional
    public boolean registerNewPlayer(Player player) {
        return playerListDao.registerNewPlayer(player);
    }

    @Transactional
    public boolean contains(Player player) {
        return playerListDao.contains(player);
    }

    @Transactional(readOnly = true)
    public Player getPlayerByName(String playerName) {
        return playerListDao.getPlayerByName(playerName);
    }

    @Transactional(readOnly = true)
    public List<Player> getPlayers() {
        return playerListDao.getAllPlayers();
    }
}
