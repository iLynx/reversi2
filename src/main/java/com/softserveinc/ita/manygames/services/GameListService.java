package com.softserveinc.ita.manygames.services;

import com.softserveinc.ita.manygames.model.Game;
import com.softserveinc.ita.manygames.model.GameListManager;
import com.softserveinc.ita.manygames.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Igor Khlaponin
 */

@Service
public class GameListService {

    @Autowired
    private GameListManager gameListManager;

    public void setGameListManager(GameListManager gameListManager) {
        this.gameListManager = gameListManager;
    }

    public boolean createGame(Player player) {
        return gameListManager.createGame(player);
    }

    public boolean deleteGame(Long id) {
        return gameListManager.deleteGame(id);
    }

    public List<Long> getCreatedGamesIDs(Player player) {
        return gameListManager.getCreatedGamesIDs(player);
    }

    public List<Long> getPlayingGamesIDs(Player player) {
        return gameListManager.getPlayingGamesIDs(player);
    }

    public List<Long> getWaitingGamesIDs(Player player) {
        return gameListManager.getWaitingGamesIDs(player);
    }

    public Game getGameById(Long id) {
        return gameListManager.getGameById(id);
    }
}
