<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Games list</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <meta http-equiv="refresh" content="3; url=/reversi/gameslist?name=${sessionScope.get("currentPlayer")}">
</head>

<body>
<h2>Games List</h2>

<div>Hi, ${sessionScope.get("currentPlayer")}! Please, choose the game</div>

<table class="table">
    <thead>
    <th>Playing games</th>
    <th>Created games</th>
    <th>Waiting games</th>
    </thead>
    <tbody>
    <tr>
        <td>
            <div> <%--Playing games--%>
                <ol>
                    <c:forEach items="${playingGames}" var="gameId">
                        <li><a href="/reversi/currentgame?id=${gameId}&name=${sessionScope.get("currentPlayer")}">
                            <c:out value="Game #${gameId}"/>
                        </a></li>
                    </c:forEach>
                </ol>
            </div>
        </td>
        <td>
            <div> <%--Created games--%>
                <ol>
                    <c:forEach items="${createdGames}" var="gameId">
                        <li><a href="/reversi/createdgame?id=${gameId}&name=${sessionScope.get("currentPlayer")}">
                            <c:out value="Game #${gameId}"/>
                        </a></li>
                    </c:forEach>
                </ol>

                <a href="/reversi/creategame?name=${sessionScope.get("currentPlayer")}">
                    <button type="button" class="btn btn-success btn-md">Create new game</button>
                </a>
            </div>
        </td>
        <td>
            <div> <%--Waiting games--%>
                <ol>
                    <c:forEach items="${waitingGames}" var="gameId">
                        <li><a href="/reversi/waitinggame?id=${gameId}&name=${sessionScope.get("currentPlayer")}">
                            <c:out value="Game #${gameId}"/>
                        </a></li>
                    </c:forEach>
                </ol>
            </div>
        </td>
    </tr>
    </tbody>
</table>

<p></p>

<%--<div style="float: left;">
    <a href="/reversi/gameslist?name=${sessionScope.get("currentPlayer")}">
        <button type="button" class="btn btn-warning">Refresh</button>
    </a>
</div>--%>

<div style="float: left;">
    <a href="/reversi/playerslist?name=${sessionScope.get("currentPlayer")}">
        <button type="button" class="btn btn-info">Get players list</button>
    </a>
</div>

<div>
    <a href="/reversi/logout">
        <button type="button" class="btn btn-danger">Log out</button>
    </a>
</div>

<script>
    $(document).ready(function(){

//        setTimeout(location.reload(), 3000);
//        updateLists();
    });

    function updateLists() {
        $.ajax({
            url: 'gameslist',
            dataType: 'text',
            success: function(){
                setTimeout(updateLists, 2000);
            },
            error: function(){
                console.log("bad connection to server");
            }
        })
    };

</script>
</body>
</html>