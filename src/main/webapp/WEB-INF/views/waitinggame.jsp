<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Do you want to play?</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <meta http-equiv="refresh" content="5; url=/reversi/gameslist?name=${sessionScope.get("currentPlayer")}">
</head>
<body>

<h2>Game #${id}</h2>

<h3>${sessionScope.get("currentPlayer")}, do you want to play with ${opponent}?</h3>
<br>

<div style="float: left">
  <a href="/reversi/currentgame?id=${id}&name=${sessionScope.get("currentPlayer")}&opponent=${opponent}">
    <button type="button" class="btn btn-success">Yes</button>
  </a>
</div>

<div>
  <a href="/reversi/gameslist?name=${sessionScope.get("currentPlayer")}">
    <button type="button" class="btn btn-danger">No</button>
  </a>
</div>

</body>
</html>
