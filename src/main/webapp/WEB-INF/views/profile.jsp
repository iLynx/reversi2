<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <style>
        .header {
            color: darkblue;
            font-size: large;
        }
        #history {
            color: darkmagenta;
            font-size: large;
        }
    </style>
</head>
<body>
<div style="float: left">
<img src="/reversi/img/profile.jpg" alt="profile pic"/>
</div>
<div>
<h2>Player info</h2>
</div>
<div id="info">
    <div class="header">Login: <span id="login"></span></div>
    <div class="header">Full Name: <span id="fullName">unknown</span></div>
    <div class="header">Email: <span id="email">unknown</span></div>
    <div class="header">Gender: <span id="gender"></span></div>
    <div class="header">Birthday: <span id="birthday"></span></div>
    <div class="header">Registration time: <span id="registration"></span></div>
    <div class="header">About: <span id="about">unknown</span></div>
</div>

<div>
    <c:if test='${playerName eq sessionScope.get("currentPlayer")}'>
    <span id="history">Games History</span>
    <c:forEach items="${gameHistories}" var="game">
        <li><a href="/reversi/gamehistory?id=${game.id}">
        <c:out value="Game #${game.id}"/>
        </a></li>
    </c:forEach>
    </c:if>
</div>

<input type="hidden" id="player" value='"${player}"'/>

<div>
  <a href="/reversi/gameslist?name=${currentPlayer}">
    <button type="button" class="btn btn-info">Check games list</button>
  </a>
</div>

<script>
    $(document).ready(function(){
        drawInfo();
    });

    function drawInfo(){
        var json = $('#player').val();
        json = json.substr(1, json.length - 2);
        json = JSON.parse(json);

        var rTime, bDate = (json.birthdayDate !== undefined)
                ? json.birthdayDate.day + '.' +
                  json.birthdayDate.month + '.' +
                  json.birthdayDate.year
                : 'unknown';

        rTime = (json.registrationTime !== undefined)
                ? json.registrationTime.date.day + '.' +
                  json.registrationTime.date.month + '.' +
                  json.registrationTime.date.year + ' at ' +
                  json.registrationTime.time.hour + ':' +
                  json.registrationTime.time.minute + ':' +
                  json.registrationTime.time.second
                : 'unknown';

        $('#login').text(json.name);
        $('#fullName').text((json.fullName !== undefined && json.fullName != null)? json.fullName : 'unknown');
        $('#email').text((json.email !== undefined && json.email != null)? json.email : 'unknown');
        $('#gender').text(json.gender);
        $('#birthday').text(bDate);
        $('#registration').text(rTime);
        $('#about').text((json.about !== undefined && json.about != null)? json.about : 'unknown');
    }

    /*function drawGamesHistories(playerName) {
        if($('#login').val() == playerName) {
            for(var )
        }
    }*/
</script>

</body>
</html>
