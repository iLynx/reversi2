<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration page</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <style>
        .registration_form {
            color: darkblue;
            font-size: medium;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        #header {
            color:#ff7f6a;
        }
        #header2 {
            color: darkblue;
        }
    </style>
</head>
<body>
<div style="float: left;">
    <%--<img src="<c:url value="${pageContext.request.contextPath}img/reversi_logo.jpg"/>" alt="reversi logo" />--%>
</div>
<div>
<h2 id="header">Registration Page</h2>
<h3 id="header2">Hi, stranger! You are not registered yet. <br>
    Please, fill the gaps</h3>
</div>

<div class="registration_form">
    <form action="registration" method="post">
        <table>
            <tr>
                <td>Login:</td>
                <td><input type="text" name="name" placeholder="type your name" autocomplete="${name}"/></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><input type="password" name="password" placeholder="type your password"/></td>
            </tr>
            <tr>
                <td>Email:</td>
                <td><input type="email" name="email"/></td>
            </tr>
            <tr>
                <td>Full Name:</td>
                <td><input type="text" name="fullName" placeholder="type your full name"/></td>
            </tr>
            <tr>
                <td>Gender:</td>
                <td><select name="gender">
                    <option disabled value="null">unknown</option>
                    <option value="MALE">Male</option>
                    <option value="FEMALE">Female</option>
                </select>
                </td>
            </tr>
            <tr>
                <td>Birthday:</td>
                <td><input type="date" name="birthdayDate"/></td>
            </tr>
            <tr>
                <td>About:</td>
                <td><input type="text" name="about" placeholder="write something about yourself"/></td>
            </tr>
        </table>
        <input type="submit" value="Register">
    </form>
</div>

</body>
</html>