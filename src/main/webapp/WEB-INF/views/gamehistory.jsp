<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Game history</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <style>
        #text {
            color: darkblue;
            font-size: medium;
        }
    </style>
</head>
<body>

<h2>Game #${id}</h2>

<div id="text">
    <div>Player1: <span>${player1.name}</span></div>
    <div>Player2: <span>${player2.name}</span></div>
    <div>StartTime: <span>${startTime}</span></div>
    <div>EndTime: <span>${endTime}</span></div>
    <div>Result: <span>${result}</span></div>
    <div><h3>TunsList:</h3>
        <table class="table">
            <thead>
            <tr>
                <th>Player</th>
                <th>Turn</th>
                <th>Time</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${turnList}" var="turn">
                <tr>
                    <td>${turn.player}</td>
                    <td>${turn.turn}</td>
                    <td>${turn.turnTime}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

</div>




  <a href="/reversi/player?playername=${sessionScope.get("currentPlayer")}&currentplayer=${sessionScope.get("currentPlayer")}">
    <button type="button" class="btn btn-info">Back to profile</button>
  </a>

  <a href="/reversi/playerslist?name=${sessionScope.get("currentPlayer")}">
    <button type="button" class="btn btn-info">Get players list</button>
  </a>

<input type="hidden" id="gameHistory" value='"${gameHistory}"'/>

<script>

    $(document).ready(function(){
        //drawGameHistory();
    });

    function drawGameHistory() {
        var json = $('#gameHistory').val();
        json = json.substr(1, json.length - 2);
        json = JSON.parse(json);

        $('#text').text('"' + json.turnList + '"');

        drawTurns(json.turnList);

    }

    function drawTurns(array) {
        $.each(array, function(i, turn) {
            drawTurn(i, turn);
        })
    }

    function drawTurn(i, turn) {
        var playerName, turnString, turnDateTime;
        turn = JSON.parse(turn);
        playerName = turn.player;
        turnString = turn.turn;
/*        turnDateTime = turn.turnTime.date.day + '.' +
        turn.turnTime.date.month + '.' +
        turn.turnTime.date.year + ' at ' +
        turn.turnTime.time.hour + ':' +
        turn.turnTime.time.minute + ':' +
        turn.turnTime.time.second*/;

//        $("#1").append('<div id=' + i +'>' + playerName + ' : ' + turnString + ' : ' + turnDateTime + '</div>')
        $("#tunrs").append('<div id=' + i +'>player: ' + playerName + ' turn: ' + turnString + ' : </div>')

    }

    /*
     "{"player":"Frodo","turn":"f4","turnTime":{"date":{"year":2016,"month":11,"day":10},"time":
     {"hour":20,"minute":29,"second":57,"nano":887000000}}},
     {"player":"Sam","turn":"d3","turnTime":{"date":{"year":2016,"month":11,"day":10},"time":
     {"hour":20,"minute":29,"second":58,"nano":62000000}}},
     {"player":"Frodo","turn":"c4","turnTime":{"date":{"year":2016,"month":11,"day":10},"time":
     {"hour":20,"minute":29,"second":58,"nano":65000000}}},
     {"player":"Sam","turn":"f5","turnTime":{"date":{"year":2016,"month":11,"day":10},"time":
     {"hour":20,"minute":29,"second":58,"nano":79000000}}}"
    * */

</script>
</body>
</html>
