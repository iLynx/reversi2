<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Congratulation</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
</head>
<body>

<c:choose>
    <c:when test="${winner} eq ${currentPlayer}">
      <h3>Congratulation! you are the winner!</h3>
    </c:when>

    <c:when test="${winner} ne ${currentPlayer}">
      <h3>You are a looser! ${winner} has won!</h3>
    </c:when>
</c:choose>


<p></p>

<div style="float: left">
  <a href="/reversi/gameslist?name=${currentPlayer}">
    <button type="button" class="btn btn-info">Back to the games list</button>
  </a>
</div>

<div>
  <a href="/reversi/">
    <button type="button" class="btn btn-danger">Log out</button>
  </a>
</div>

</body>
</html>
