
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Created game</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
</head>
<body>

<h2>Game # ${id}</h2>
<h3>${name} vs. nobody</h3>

<br/>
No one connected yet
<br/>
<br/>

<div>
  <a href="/reversi/gameslist?name=${name}">
    <button type="button" class="btn btn-info">Return to games list</button>
  </a>
</div>
</body>
</html>
