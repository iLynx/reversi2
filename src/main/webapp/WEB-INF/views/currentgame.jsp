<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Current Game</title>
  <link rel="stylesheet" href="/reversi/styles/fieldstyle.css"/>
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="/reversi/js/field.js"></script>
  <script src="/reversi/js/currentpageonload.js"></script>
  <script src="/reversi/js/ajaxRequestLoop.js"></script>
  <script src="/reversi/js/getCurrentGameStatus.js"></script>
  <script src="/reversi/js/onclickEvent.js"></script>
  <script src="/reversi/js/getResultCode.js"></script>
  <style>
    #player1 {
      color: lawngreen;
    }
    #player2 {
      color: orangered;
    }
    .status {
      font-size: medium;
    }
  </style>
</head>
<body>


<div><h2 style="color: #81b2ff">Reversi game #${gameNumber}</h2></div>
<div><h3><span id="player1">${currentPlayer}</span> vs. <span id="player2">${opponent}</span></h3></div>
<%--<div>${currentPlayer}, you are playing with <b>${pawnColor}</b></div>--%>
<div class="status"><span id="status"></span> ${currentPlayer}, you are playing with <b>${pawnColor}</b></div>

<div id="turnfield"><%--Turn--%>
  <form id="turnForm" action="maketurn" method="post">
    <input id="turn" type="text" name="turn" placeholder="please, type your turn"/>
    <input type="hidden" name = "id" value="${gameNumber}"/>
    <input type="hidden" name = "currentPlayer" value="${currentPlayer}"/>
    <input type="hidden" name = "opponent" value="${opponent}"/>
    <input type="hidden" name = "matrix" value="${matrix}"/>
    <input type="submit" name = "submit" value="Make turn"/>
  </form>
</div>

<div>
  <div style="float: left">Result code: </div>
  <span id="resultCode"></span>
</div>


<div id="field"> <%--Field--%>
</div>

<input type="hidden" id="idValue" value='${gameNumber}'/>
<input type="hidden" id="matrix" value='"${matrix}"'/>


<div>
  <a href="/reversi/gameslist?name=${currentPlayer}">
    <button type="button" class="btn btn-info">Back to the games list</button>
  </a>
</div>

<div id="fieldbackground">
  <img src="/reversi/img/chessboard_background.jpg" alt="board background"/>
</div>

</body>
</html>
