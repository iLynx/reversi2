<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Players list</title>
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
</head>
<body>

<h2>Players list</h2>

<div>
  <ol>
    <c:forEach items="${playersList}" var="player">
      <li><a href="/reversi/player?playername=${player.name}&currentplayer=${currentPlayer}">
        <c:out value="${player.name}"/>
      </a></li>
    </c:forEach>
  </ol>
</div>

<a href="/reversi/gameslist?name=${currentPlayer}">
  <button type="button" class="btn btn-info">Get games list</button>
</a>

</body>
</html>
