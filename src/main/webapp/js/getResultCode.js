function refreshResultCode() {
    $.ajax({
        method: 'GET',
        url: 'getResultCode',
        data: 'id=' + $('#idValue').val(),
        success: function (data) {
            var resultCode;

            switch (data) {
                case '0': {
                    resultCode = 'OK';
                    $('#resultCode').text(resultCode).css('background-color', 'greenyellow');
                    break;
                }
                case '1': {
                    resultCode = 'BAD_PLAYER';
                    $('#resultCode').text(resultCode).css('background-color', 'red');
                    break;
                }
                case '2': {
                    resultCode = 'BAD_FIRST_PLAYER_ORDER';
                    $('#resultCode').text(resultCode).css('background-color', 'red');
                    break;
                }
                case '3': {
                    resultCode = 'BAD_SECOND_PLAYER_ORDER';
                    $('#resultCode').text(resultCode).css('background-color', 'red');
                    break;
                }
                case '4': {
                    resultCode = 'BAD_TURN_ORDER';
                    $('#resultCode').text(resultCode).css('background-color', 'red');
                    break;
                }
                case '5': {
                    resultCode = 'BAD_TURN_SYNTAX';
                    $('#resultCode').text(resultCode).css('background-color', 'red');
                    break;
                }
                case '6': {
                    resultCode = 'BAD_TURN_LOGIC';
                    $('#resultCode').text(resultCode).css('background-color', 'red');
                    break;
                }
                case '7': {
                    resultCode = 'BAD_TURN_FOR_FINISHED_GAME';
                    $('#resultCode').text(resultCode).css('background-color', 'red');
                    break;
                }
                case '8': {
                    resultCode = 'BAD_TURN_FOR_NOT_STARTED_GAME';
                    $('#resultCode').text(resultCode).css('background-color', 'red');
                    break;
                }
                default: {
                    resultCode = 'UNKNOWN STATUS';
                    $('#resultCode').text(resultCode).css('background-color', 'yellow');
                }
            }

        },
        error: function (errorThrown) {
            console.log("Error: " + errorThrown);
        }
    });
}
