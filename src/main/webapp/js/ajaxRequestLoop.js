'use strict'

    function ajaxRequestLoop() {
        $.ajax({
            url: 'updatefield',
            data: 'id=' + $('#idValue').val(),
            dataType: 'text',
            success: function(responseText){
                $('#matrix').val('\"' + responseText + '\"');
                clearField();
                drawField();
                syncField(repackField());
                refreshGameStatus();
            },
            error: function(responseText){
                console.log("bad connection to server");
            }
        })
    }

