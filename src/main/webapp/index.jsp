<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Welcome page</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <style>
        #message {
            color: red;
        }
        .definition {
            color: darkblue;
            font-size: medium;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            height: auto;
            width: 1000px;
            text-align: justify;
        }
    </style>
</head>
<body>
<div>
    <div style="float: left">
    <img src="<c:url value="img/reversi_logo.jpg"/>" alt="reversi logo" />
    </div>
    <h2 style="color: #ff7f6a;">Welcome to the Reversi game world!</h2>
</div>


<p>Please, log in</p>

<form role="form" action="login" method="post">
    <div class="form-group">
    Login:<input type="text" name="name" placeholder="type your name"/>
    Password:<input type="password" name="password" placeholder="type your password"/>
    </div>
    <input type="submit" value="Go into">
</form>

<div id="message">${message}</div>

<a href="registration">
    <button type="button" class="btn btn-success btn-md">Registration</button>
</a>

<br>

<div class="definition">
    <p><b>Reversi</b> is a strategy board game for two players, played on an 8x8 uncheckered board.
        There are sixty-four identical game pieces called disks (often spelled "discs"),
        which are light on one side and dark on the other.
        Players take turns placing disks on the board with their assigned color facing up.
        During a play, any disks of the opponent's color that are in a straight line and bounded
        by the disk just placed and another disk of the current player's color are turned over
        to the current player's color.
    </p>

    <p>The object of the game is to have the majority of disks turned to display your color
        when the last playable empty square is filled.</p>
</div>

</body>
</html>
